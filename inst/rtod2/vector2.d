module rtod2.vector2;

pragma(lib, "libgretl-1.0");
pragma(lib, "libR");

import rtod2.gretl2, rtod2.matrix2, rtod2.r2, rtod2.utils2;
import std.conv, std.stdio;

struct GretlVector {
  DoubleMatrix mat;
  DoubleMatrix * matptr;
  bool free = true;

  this(int r) {
    matptr = gretl_matrix_alloc(r, 1);
    mat.rows = r;
    mat.cols = 1;
    mat.val = matptr.val;
    mat.info = matptr.info;
  }

  this(this) {
    free = false;
  }

  double * ptr() {
    return mat.val;
  }

  ~this() {
    if (free) {
      gretl_matrix_free(matptr); 
    }
  }

  double opIndex(int r) {
    assertR(r < mat.rows, "Index out of range: index on GretlVector is too large");
    return mat.val[r];
  }

  void opIndexAssign(double v, int r) {
    assertR(r < mat.rows, "Index out of range: index on GretlVector is too large");
    mat.val[r] = v;
  }

  GretlVector opSlice(int i, int j) {
    assertR(j < mat.rows, "Index out of range: index on GretlVector slice is too large");
    assertR(i < j, "First index has to be less than second index");
    GretlVector result;
    result.mat.rows = j-i;
    result.mat.cols = 1;
    result.mat.val = &mat.val[i];
    result.mat.info = mat.info;
    result.matptr = matptr;
    result.free = false;
    return result;
  }

  void print() {
    foreach(val; this) {
      writeln(val);
    }
  }

  bool empty() {
    return mat.rows == 0;
  }

  double front() {
    return this[0];
  }

  void popFront() {
    mat.val = &mat.val[1];
    mat.rows -= 1;
  }

  RVector opCast(T: RVector)() {
    auto result = RVector(mat.rows);
    foreach(int ii; 0..to!int(mat.rows)) {
      result[ii] = this[ii];
    }
    return result;
  }

  double[] array() {
    double[] result;
    result.reserve(mat.rows);
    foreach(val; this) {
      result ~= val;
    }
    return result;
  }

  alias mat this;
}

struct RVector {
  DoubleMatrix mat;
  Robj robj;
  bool unprotect = true;

  this(int r) {
    Rf_protect(robj = Rf_allocVector(14,r));
    mat.rows = r;
    mat.cols = 1;
    mat.val = REAL(robj);
  }

  this(Robj rv) {
    assertR(isVector(rv), "In RVector constructor: Cannot convert non-vector R object to RVector");
    assertR(isNumeric(rv), "In RVector constructor: Cannot convert non-numeric R object to RVector");
    robj = rv;
    mat.rows = rv.length;
    mat.cols = 1;
    mat.val = REAL(rv);
    unprotect = false;
  }

  this(T)(T v) {
    Rf_protect(robj = Rf_allocVector(14,to!int(v.length)));
    mat.rows = to!int(v.length);
    mat.cols = 1;
    mat.val = REAL(robj);
    foreach(ii; 0..to!int(v.length)) {
      mat.val[ii] = v[ii];
    }
  }

  this(this) {
    unprotect = false;
  }

  double * ptr() {
    return mat.val;
  }

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj); 
    }
  }

  double opIndex(int r) {
    assertR(r < mat.rows, "Index out of range: index on RVector is too large");
    return mat.val[r];
  }

  void opIndexAssign(double v, int r) {
    assertR(r < mat.rows, "Index out of range: index on RVector is too large");
    mat.val[r] = v;
  }

  void opAssign(T)(T x) {
    assertR(x.length == mat.rows, "Cannot assign to RVector from an object with the wrong length");
    foreach(ii; 0..to!int(x.length)) {
      this[ii] = x[ii];
    }
  }

  RVector opSlice(int i, int j) {
    assertR(j < mat.rows, "Index out of range: index on RVector slice is too large");
    assertR(i < j, "First index has to be less than second index");
    RVector result;
    result.mat.rows = j-i;
    result.mat.cols = 1;
    result.mat.val = &mat.val[i];
    result.mat.info = mat.info;
    result.robj = robj;
    result.unprotect = false;
    return result;
  }

  GretlVector opCast(T: GretlVector)() {
    auto result = GretlVector(mat.rows);
    foreach(ii; 0..to!int(mat.rows)) {
      result[ii] = this[ii];
    }
    return result;
  }

  void print() {
    foreach(val; this) {
      writeln(val);
    }
  }

  bool empty() {
    return mat.rows == 0;
  }

  double front() {
    return this[0];
  }

  void popFront() {
    mat.val = &mat.val[1];
    mat.rows -= 1;
  }

  double[] array() {
    double[] result;
    result.reserve(mat.rows);
    foreach(val; this) {
      result ~= val;
    }
    return result;
  }

  alias mat this;
}

struct RIntVector {
  Robj robj;
  ulong length;
  int * ptr;
  bool unprotect = true;

  this(int r) {
    Rf_protect(robj = Rf_allocVector(13, r));
    length = r;
    ptr = INTEGER(robj);
  }

  this(int[] v) {
    Rf_protect(robj = Rf_allocVector(13, to!int(v.length)));
    length = to!int(v.length);
    ptr = INTEGER(robj);
    foreach(int ii, val; v) {
      this[ii] = val;
    }
  }

  this(Robj rv) {
    assertR(isVector(rv), "In RVector constructor: Cannot convert non-vector R object to RVector");
    assertR(isInteger(rv), "In RVector constructor: Cannot convert non-integer R object to RVector");
    robj = rv;
    length = rv.length;
    ptr = INTEGER(rv);
    unprotect = false;
  }

  this(this) {
    unprotect = false;
  }

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj);
    }
  }

  int opIndex(int obs) {
    assertR(obs < length, "Index out of range: index on RIntVector is too large");
    return ptr[obs];
  }

  void opIndexAssign(int val, int obs) {
    assertR(obs < length, "Index out of range: index on RIntVector is too large");
    ptr[obs] = val;
  }

  void opAssign(int[] v) {
    foreach(int ii, val; v) {
      this[ii] = val;
    }
  }

  RIntVector opSlice(int i, int j) {
    assertR(j < length, "Index out of range: index on RIntVector slice is too large");
    assertR(i < j, "First index on RIntVector slice has to be less than the second index");
    RIntVector result;
    result.robj = this.robj;
    result.length = j-i;
    result.ptr = &this.ptr[i];
    result.unprotect = false;
    return result;
  }

  int[] array() {
    int[] result;
    result.reserve(length);
    foreach(val; this) {
      result ~= val;
    }
    return result;
  }

  void print() {
    foreach(val; this) {
      writeln(val);
    }
  }

  bool empty() {
    return length == 0;
  }

  int front() {
    return this[0];
  }

  void popFront() {
    ptr = &ptr[1];
    length -= 1;
  }
}
