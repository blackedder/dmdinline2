module rtod2.dynmodel2;

pragma(lib, "libgretl-1.0");
pragma(lib, "libR");

import rtod2.gretl2, rtod2.matrix2, rtod2.r2, rtod2.reg2, rtod2.utils2, rtod2.vector2;
import std.algorithm, std.conv, std.stdio;

alias MTS = TS[string];

struct Lag {
  string name;
  int lag = 0;
}

// Comment T is a type that can be converted to int
TS[] Lags(T)(string name, T k) {
  return Lag(name, to!int(k));
}

TS[] Lags(T)(string name, T[] ks) {
  Lag[] result;
  foreach(k; ks) {
    result ~= Lag(name, to!int(k));
  }
}

//Comment **Warning:** start and end are not binding. They are the earliest and latest *potential* start and end dates, respectively.
struct DynModel {
  string lhs;
  Lag[] rhs;
  bool intercept = true;
  ulong start = 0;
  ulong end = ulong.max;

  alias rhs this;
}

TS lag(TS x, ulong k) {
  TS result;
  result.data = x.data;
  result.start = x.start;
  result.frequency = x.frequency;
  return result;
}

TS lag(Robj rx, ulong k) {
  auto x = TS(rx);
  TS result;
  result.data = x.data;
  result.start = x.start;
  result.frequency = x.frequency;
  return result;
}

private TS[] array(NamedList dataset, DynModel mod) {
  TS[] result;
  foreach (var; mod.rhs) {
    result ~= lag(dataset[var.name], var.lag);
  }
  return result;
}

private ulong[2] commonDates(TS lhs, TS[] rhs) {
  ulong s = lhs.start;
  ulong e = lhs.end;
  foreach(val; rhs) {
    if (val.start > s) {
      s = val.start;
    }
    if (val.end < e) {
      e = val.end;
    }
  }
  return [s, e];
}

private ulong[2] commonDates(Robj lhs, TS[] rhs) {
  return commonDates(TS(lhs), rhs);
}

struct DynData {
  TS lhs;
  GretlMatrix rhs;
  ulong start;
  ulong end;

  this(NamedList dataset, DynModel mod) {
    TS[] rhsvars = dataset.array(mod);

    //Comment Use start and end from mod if they are binding
    ulong[2] cd = commonDates(dataset[mod.lhs], rhsvars);
    ulong[2] dates = [max(cd[0], mod.start), min(cd[0], mod.end)];
    
    //Comment Place the data into a matrix using the common dates or the dates in mod if they are binding
    auto result = GretlMatrix(to!int(dates[1] - dates[0] + 1), to!int(rhsvars.length + mod.intercept));
    foreach(int ii, var; rhsvars) {
      Col(result, ii) = var.window(dates);
    }
    if (mod.intercept) {
      Col(result, result.cols - 1) = 1.0;
    }
    rhs = result;
    lhs = TS(dataset[mod.lhs]);
    start = dates[0];
    end = dates[1];
  }
}

OlsFit lm(NamedList dataset, DynModel mod) {
  auto ds = DynData(dataset, mod);
  return rtod2.reg2.lm(ds.lhs, ds.rhs);
}

struct DynFit {
  GretlVector coef;
  GretlMatrix vcv;
  TS resids;
  double s2;
  ulong start;
  ulong frequency;
}

double sse(DynFit fit) {
  double result = 0.0;
  foreach(e; fit.resids) {
    result += e*e;
  }
  return result;
}

struct TS {
  DoubleMatrix data;
  ulong start;
  ulong frequency;

  this(DoubleMatrix v, ulong[2] s, ulong f=1) {
    assertR(v.cols == 1, "Cannot create a TS struct out of a matrix");
    data = v;
    start = s[0]*f + s[1];
    frequency = f;
  }

  this(DoubleMatrix v, ulong s, ulong f=1) {
    data = v;
    start = s;
    frequency = f;
  }

  this(double * ptr, ulong s, ulong length, ulong f) {
    data.cols = 1;
    data.rows = to!int(length);
    data.val = ptr;
    start = s;
    frequency = f;
  }

  this(Robj x) {
    data.cols = 1;
    data.rows = Rf_length(x);
    data.val = REAL(x);
    ulong[3] tsprop = x.tsp;
    start = tsprop[0];
    frequency = tsprop[2];
  }

  double opIndex(ulong d) {
    assertR(d >= start, "Index on TS is before the start of the series");
    assertR(d <= this.end, "Index on TS is after the end of the series");
    return data[to!int(d-start), 0];
  }

  double opIndex(ulong y, ulong p) {
    return this[to!int(y*frequency + p)];
  }

  void opIndexAssign(double val, ulong d) {
    assertR(d >= start, "Index on TS is before the start of the series");
    assertR(d <= this.end, "Index on TS is after the end of the TS");
    data[to!int(d-start), 0] = val;
  }

  TS opSlice(ulong s, ulong e) {
    return TS(&data.val[s-start], s, e-s, frequency);
  }

  TS opSlice(ulong[2] s, ulong[2] e) {
    return opSlice(s[0]*frequency+s[1], e[0]*frequency+e[1]);
  }

  ulong opDollar() {
    return start + data.rows;
  }

  ulong length() {
    return data.rows;
  }

  ulong end() {
    return start+data.rows-1;
  }

  ulong[2] startDate() {
    return [start/frequency, start%frequency];
  }

  ulong[2] endDate() {
    return [this.end/frequency, this.end%frequency];
  }

  bool empty() {
    return data.rows == 0;
  }

  double front() {
    return data[0,0];
  }

  void popFront() {
    data.val = &data.val[1];
    data.rows -= 1;
  }

  void print() {
    writeln("---------------------");
    write("Start:     ", this.startDate, "\n");
    write("End:       ", this.endDate, "\n");
    write("Frequency: ", frequency, "\n");
    writeln("---------------------");
    data.print();
  }

  //Comment alias this leads to these operations returning a GretlMatrix. We need to add the TS properties.
  TS opBinary(string op)(double a) {
    static if (op == "+") {
      return TS(this.data+a, this.start, this.frequency);
   }
    static if (op == "-") {
      return TS(this.data-a, this.start, this.frequency);
    }
    static if (op == "*") {
      return TS(this.data*a, this.start, this.frequency);
    }
    static if (op == "/") {
      return TS(this.data/a, this.start, this.frequency);
    }
  }

  TS opBinaryRight(string op)(double a) {
    static if (op == "+") { 
      return TS(a+this.data, this.start, this.frequency); 
    }
    static if (op == "-") {
      return TS(a-this.data, this.start, this.frequency);
    }
    static if (op == "*") { 
      return TS(a*this.data, this.start, this.frequency); 
    }
    static if (op == "/") {
      return TS(a/this.data, this.start, this.frequency);
    }
  }

  //Comment Only do + and -. We can't do multiplication unless we redefine * to be element-by-element.
  TS opBinary(string op)(TS y) {
    static if (op == "+") {
      assertR(this.length == y.length, "Cannot add TS objects of different lengths");
      assertR(this.frequency == y.frequency, "Cannot add TS objects of different frequencies");
      assertR(this.start == y.start, "Cannot add TS objects with different start dates");
      return TS(this.data+y.data, this.start, this.frequency);
    }
    static if (op == "-") {
      assertR(this.length == y.length, "Cannot take the difference of TS objects of different lengths");
      assertR(this.frequency == y.frequency, "Cannot take the difference of TS objects of different frequencies");
      assertR(this.start == y.start, "Cannot take the difference of TS objects with different start dates");
      return TS(this.data-y.data, this.start, this.frequency);
    }
  }
 
  alias data this;
}

TS window(TS x, ulong t0, ulong t1) {
  assertR(t0 >= x.start, "Cannot start window before the start date of the series");
  assertR(t1 <= x.end, "Cannot end window after the end date of the series");
  return x[t0..t1+1];
}

TS window(TS x, ulong[2] d) {
  return window(x, d[0], d[1]);
}
