module rtod2.reg2;

pragma(lib, "libgretl-1.0");
pragma(lib, "libR");

import rtod2.gretl2, rtod2.matrix2, rtod2.r2, rtod2.utils2, rtod2.vector2;

struct OlsFit {
  GretlVector coef;
  GretlMatrix vcv;
  GretlVector resids;
  double s2;
}

double sse(OlsFit fit) {
  double result = 0.0;
  foreach(e; fit.resids) { 
    result += e*e; 
  }
  return result;
}

OlsFit lm(DoubleMatrix y, DoubleMatrix x) {
  assertR(y.rows == x.rows, "y and x have different number of rows");
  assertR(y.cols == 1, "y can only have one column");
  auto coef = GretlVector(x.cols);
  auto vcv = GretlMatrix(x.cols, x.cols);
  auto resids = GretlVector(y.rows);
  double s2;
  gretl_matrix_ols(y.matptr, x.matptr, coef.matptr, vcv.matptr, resids.matptr, &s2);
  return OlsFit(coef, vcv, resids, s2);
}

OlsFit lmSubsample(DoubleMatrix y, DoubleMatrix x, int r1, int r2) {
  return lm(y.copyRows(r1, r2+1), x.copyRows(r1, r2+1));
}
