module rtod2.random2;

pragma(lib, "libgretl-1.0");
pragma(lib, "libR");

import rtod2.gretl2, rtod2.matrix2, rtod2.vector2, rtod2.r2;

void setSeed(uint seed) { 
  gretl_rand_set_seed(seed);
}

uint getSeed() { 
  return gretl_rand_get_seed();
}

bool usingBM() { 
  return gretl_rand_get_box_muller() > 0;
}

void useBM() { 
  gretl_rand_set_box_muller(1);
}

void useZiggurat() { 
  gretl_rand_set_box_muller(0);
}

uint genInteger(uint k) { 
  return gretl_rand_int_max(k);
}

GretlVector runif(int n, double min=0.0, double max=1.0) {
  auto result = GretlVector(n);
  gretl_rand_uniform_minmax(result.ptr, 0, n-1, min, max);
  return result;
}

double runif() { 
  return gretl_rand_01();
}

GretlVector rnorm(int n, double mean=0.0, double sd=1.0) {
  auto result = GretlVector(n);
  gretl_rand_normal_full(result.ptr, 0, n-1, mean, sd);
  return result;
}

double rnorm() { 
  return gretl_one_snormal(); 
}

double rnorm(double mean, double sd) { 
  return mean + sd*gretl_one_snormal();
}

GretlMatrix rmvnorm(DoubleMatrix mu, DoubleMatrix V) {
  assertR(mu.cols == 1, "rmvnorm: mu needs to have one column");
  assertR(mu.rows == V.rows, "rmvnorm: mu and v need to have the same number of rows");
  assertR(V.rows == V.cols, "rmvnorm: v needs to be square");
  return mu + chol(V)*rnorm(mu.rows); // rnorm returns GretlVector, which aliases to a DoubleMatrix
}
