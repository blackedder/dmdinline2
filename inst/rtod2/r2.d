module rtod2.r2;

pragma(lib, "libgretl-1.0");
pragma(lib, "libR");

import rtod2.matrix2, rtod2.utils2, rtod2.vector2;
import std.array, std.conv, std.exception, std.math, std.stdio, std.string;
import std.algorithm;

void printR(Robj x) {
  Rf_PrintValue(x);
}

void assertR(bool test, string msg) {
  if (!test) { 
    Rf_error( ("Error in D code: " ~ msg ~ "\0").dup.ptr );
  }
}

int length(Robj x) {
  return Rf_length(x);
}

bool isVector(Robj x) {
  return to!bool(Rf_isVector(x));
}

bool isMatrix(Robj x) {
  return to!bool(Rf_isMatrix(x));
}

bool isNumeric(Robj x) {
  return to!bool(Rf_isNumeric(x));
}

bool isInteger(Robj x) {
  return to!bool(Rf_isInteger(x));
}

struct RList {
  Robj robj; // This is the entire list, not an individual element
  int length;
  bool unprotect;
  private int counter = 0;

  this(int n) {
    Rf_protect(robj = Rf_allocVector(19, n));
    length = n;
    unprotect = true;
  }

  this(Robj v) {
    robj = v;
    length = v.length;
    unprotect = false;
  }

  this(Robj[] v) {
    Rf_protect(robj = Rf_allocVector(19, to!int(v.length)));
    length = to!int(v.length);
    unprotect = true;
    foreach(int ii, val; v) {
      SET_VECTOR_ELT(robj, ii, val);
    }
  }

  //Comment Allows sending an RList as an argument to a function without being finalized.
  this(this) {
    unprotect = false;
  }

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj);
    }
  }

  Robj opIndex(int ii) {
    assertR(ii < length, "RList index has to be less than the number of elements");
    return VECTOR_ELT(robj, ii);
  }

  void opIndexAssign(Robj r, int ii) {
    assertR(ii < length, "RList index has to be less than the number of elements");
    SET_VECTOR_ELT(robj, ii, r);
  }

  bool empty() {
    return counter == length;
  }

  Robj front() {
    return this[counter];
  }

  void popFront() {
    counter -= 1;
  }
}

string[] names(Robj x) {
  return stringArray(getAttrib(x, "names"));
}

struct NamedRobj {
  Robj robj;
  string name;
}

//Comment Cannot do this using an associative array. That would give much faster access by name but you would lose the ability to access by index. Lists are small enough that it shouldn't matter.
struct NamedList {
  NamedRobj[] data;
  
  this(Robj x) {
    foreach(int ii, name; x.names) {
       data ~= NamedRobj(VECTOR_ELT(x, ii), name);
    }
  }

  Robj opIndex(int ii) {
    assertR(ii < data.length, "NamedList index is greater than the length");
    return data[ii].robj;
  }

  Robj opIndex(string name) {
    auto ind = countUntil!"a.name == b"(data, name);
    if (ind == -1) { assertR(false, "No element in the list with the name " ~ name); }
    return data[ind].robj;
  }

  void opIndexAssign(Robj r, long ii) {
    assertR(ii < data.length, "NamedList index is greater than the length");
    data[ii].robj = r;
  }

  //Comment If the element doesn't exist, it is added using that name, otherwise that element is mutated
  void opIndexAssign(Robj r, string name) {
    auto ind = countUntil!"a.name == b"(data, name);
    if (ind == -1) {
      data ~= NamedRobj(r, name);
    } else {
      data[ind].robj = r;
    }
  }

  Robj robj() {
    auto rl = RList(to!int(data.length));
    auto names = RStringVector(to!int(data.length));
    foreach(int ii, val; data) {
      rl[ii] = val.robj;
      names[ii] = val.name;
    }
    auto result = rl.robj;
    setAttrib(result, "names", names.robj);
    return result;
  }

  void print() {
    foreach(val; data) {
      writeln(val.name, ":");
      printR(val.robj);
    }
  }
}

//Comment This converts a *single* string into a D string. It is more efficient than using STRING_ELT but cannot handle multiple strings.
string asString(Robj cstr) {
  return to!string(R_CHAR(cstr));
}

//Comment This pulls the iith element from an R string vector and returns it as a D string.
string asString(Robj sv, int ii) {
  return to!string(R_CHAR(STRING_ELT(sv, ii)));
}

string[] stringArray(Robj sv) {
  string[] result;
  foreach(ii; 0..Rf_length(sv)) {
    result ~= asString(sv, ii);
  }
  return result;
}

//Comment No constructor taking Robj as an argument because it's only one string, so use asString instead. You shouldn't be using this library if you need something more efficient than that.
struct RString {
  Robj robj;
  bool unprotect = true;
  
  this(string str) {
    Rf_protect(robj = Rf_allocVector(16, 1));
    SET_STRING_ELT(robj, 0, Rf_mkChar((str ~ "\0").dup.ptr));
  }

  this(this) {
    unprotect = false;
  }

  alias robj this;

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj);
    }
  }
}

//Comment This is for passing an array of strings from D to R.
struct RStringVector {
  Robj robj;
  bool unprotect = true;

  this(int n) {
    Rf_protect(robj = Rf_allocVector(16, n));
  }

  this(string[] sv) {
    Rf_protect(robj = Rf_allocVector(16, to!int(sv.length)));
    foreach(int ii, s; sv) {
      this[ii] = s;
    }
  }

  this(this) {
    unprotect = false;
  }

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj);
    }
  }

  void opIndexAssign(string s, int ii) {
    assertR(ii < Rf_length(robj), "Index on RStringVector has to be less than the number of elements");
    SET_STRING_ELT(robj, ii, Rf_mkChar((s ~ "\0").dup.ptr));
  }
}

Robj getAttrib(Robj x, string attr) {
  return Rf_getAttrib(x, RString(attr));
}

Robj getAttrib(Robj x, RString attr) {
  return Rf_getAttrib(x, attr);
}

void setAttrib(Robj x, string attr, Robj val) {
  Rf_setAttrib(x, RString(attr), val);
}

void setAttrib(Robj x, RString attr, Robj val) {
  Rf_setAttrib(x, attr, val);
}

Robj robj(double x) {
  return Rf_ScalarReal(x);
}

Robj robj(double[] v) {
  return RVector(v).robj;
}

Robj robj(int x) {
  return Rf_ScalarInteger(x);
}

Robj robj(string s) {
  return RString(s).robj;
}

Robj robj(string[] sv) {
  return RStringVector(sv).robj;
}

ulong[3] tsp(Robj rv) {
  auto tsprop = RVector(getAttrib(rv, "tsp"));
  ulong[3] result;
  result[0] = lround(tsprop[0]*tsprop[2])+1;
  result[1] = lround(tsprop[1]*tsprop[2])+1;
  result[2] = lround(tsprop[2]);
  return result;
}

extern (C) {
  double * REAL(Robj x);
  int * INTEGER(Robj x);
  const(char) * R_CHAR(Robj x);
  int * LOGICAL(Robj x);
  Robj STRING_ELT(Robj x, int i);
  Robj VECTOR_ELT(Robj x, int i);
  Robj SET_VECTOR_ELT(Robj x, int i, Robj v);
  void SET_STRING_ELT(Robj x, int i, Robj v);
  int Rf_length(Robj x);
  int Rf_ncols(Robj x);
  int Rf_nrows(Robj x);
  extern __gshared Robj R_NilValue;
  alias RNil = R_NilValue;
  
  void Rf_PrintValue(Robj x);
  int Rf_isArray(Robj x);
  int Rf_isInteger(Robj x);
  int Rf_isList(Robj x);
  int Rf_isLogical(Robj x);
  int Rf_isMatrix(Robj x);
  int Rf_isNull(Robj x);
  int Rf_isNumber(Robj x);
  int Rf_isNumeric(Robj x);
  int Rf_isReal(Robj x);
  int Rf_isVector(Robj x);
  int Rf_isVectorList(Robj x);
  Robj Rf_protect(Robj x);
  Robj Rf_unprotect(int n);
  Robj Rf_unprotect_ptr(Robj x);
  Robj Rf_listAppend(Robj x, Robj y);
  Robj Rf_duplicate(Robj x);
  double Rf_asReal(Robj x);
  int Rf_asInteger(Robj x);
  Robj Rf_ScalarReal(double x);
  Robj Rf_ScalarInteger(int x);
  Robj Rf_getAttrib(Robj x, Robj attr);
  Robj Rf_setAttrib(Robj x, Robj attr, Robj val);
  Robj Rf_mkChar(const char * str);
  void Rf_error(const char * msg);
    
  // type is 0 for NILSXP, 13 for integer, 14 for real, 19 for VECSXP
  Robj Rf_allocVector(uint type, int n);
  Robj Rf_allocMatrix(uint type, int rows, int cols);
        
  // I don't use these, and don't know enough about them to mess with them
  // They are documented in the R extensions manual.
  double gammafn(double);
  double lgammafn(double);
  double lgammafn_sign(double, int *);
  double digamma(double);
  double trigamma(double);
  double tetragamma(double);
  double pentagamma(double);
  double beta(double, double);
  double lbeta(double, double);
  double choose(double, double);
  double lchoose(double, double);
  double bessel_i(double, double, double);
  double bessel_j(double, double);
  double bessel_k(double, double, double);
  double bessel_y(double, double);
  double bessel_i_ex(double, double, double, double *);
  double bessel_j_ex(double, double, double *);
  double bessel_k_ex(double, double, double, double *);
  double bessel_y_ex(double, double, double *);
        
        
  /** Calculate exp(x)-1 for small x */
  double expm1(double);
        
  /** Calculate log(1+x) for small x */
  double log1p(double);
        
  /** Returns 1 for positive, 0 for zero, -1 for negative */
  double sign(double x);
        
  /** |x|*sign(y)
   *  Gives x the same sign as y
   */   
  double fsign(double x, double y);
        
  /** R's signif() function */
  double fprec(double x, double digits);
        
  /** R's round() function */
  double fround(double x, double digits);
        
  /** Truncate towards zero */
  double ftrunc(double x);
        
  /** Same arguments as the R functions */ 
  double dnorm4(double x, double mu, double sigma, int give_log);
  double pnorm(double x, double mu, double sigma, int lower_tail, int log_p);
  double qnorm(double p, double mu, double sigma, int lower_tail, int log_p);
  void pnorm_both(double x, double * cum, double * ccum, int i_tail, int log_p); /* both tails */
  /* i_tail in {0,1,2} means: "lower", "upper", or "both" :
     if(lower) return *cum := P[X <= x]
     if(upper) return *ccum := P[X > x] = 1 - P[X <= x] */

  /** Same arguments as the R functions */ 
  double dunif(double x, double a, double b, int give_log);
  double punif(double x, double a, double b, int lower_tail, int log_p);
  double qunif(double p, double a, double b, int lower_tail, int log_p);

  /** These do not allow for passing argument rate as in R 
      Confirmed that otherwise you call them the same as in R */
  double dgamma(double x, double shape, double scale, int give_log);
  double pgamma(double q, double shape, double scale, int lower_tail, int log_p);
  double qgamma(double p, double shape, double scale, int lower_tail, int log_p);
        
  /** Unless otherwise noted from here down, if the argument
   *  name is the same as it is in R, the argument is the same.
   *  Some R arguments are not available in C */
  double dbeta(double x, double shape1, double shape2, int give_log);
  double pbeta(double q, double shape1, double shape2, int lower_tail, int log_p);
  double qbeta(double p, double shape1, double shape2, int lower_tail, int log_p);

  /** Use these if you want to set ncp as in R */
  double dnbeta(double x, double shape1, double shape2, double ncp, int give_log);
  double pnbeta(double q, double shape1, double shape2, double ncp, int lower_tail, int log_p);
  double qnbeta(double p, double shape1, double shape2, double ncp, int lower_tail, int log_p);

  double dlnorm(double x, double meanlog, double sdlog, int give_log);
  double plnorm(double q, double meanlog, double sdlog, int lower_tail, int log_p);
  double qlnorm(double p, double meanlog, double sdlog, int lower_tail, int log_p);

  double dchisq(double x, double df, int give_log);
  double pchisq(double q, double df, int lower_tail, int log_p);
  double qchisq(double p, double df, int lower_tail, int log_p);

  double dnchisq(double x, double df, double ncp, int give_log);
  double pnchisq(double q, double df, double ncp, int lower_tail, int log_p);
  double qnchisq(double p, double df, double ncp, int lower_tail, int log_p);

  double df(double x, double df1, double df2, int give_log);
  double pf(double q, double df1, double df2, int lower_tail, int log_p);
  double qf(double p, double df1, double df2, int lower_tail, int log_p);

  double dnf(double x, double df1, double df2, double ncp, int give_log);
  double pnf(double q, double df1, double df2, double ncp, int lower_tail, int log_p);
  double qnf(double p, double df1, double df2, double ncp, int lower_tail, int log_p);

  double dt(double x, double df, int give_log);
  double pt(double q, double df, int lower_tail, int log_p);
  double qt(double p, double df, int lower_tail, int log_p);

  double dnt(double x, double df, double ncp, int give_log);
  double pnt(double q, double df, double ncp, int lower_tail, int log_p);
  double qnt(double p, double df, double ncp, int lower_tail, int log_p);

  double dbinom(double x, double size, double prob, int give_log);
  double pbinom(double q, double size, double prob, int lower_tail, int log_p);
  double qbinom(double p, double size, double prob, int lower_tail, int log_p);

  double dcauchy(double x, double location, double scale, int give_log);
  double pcauchy(double q, double location, double scale, int lower_tail, int log_p);
  double qcauchy(double p, double location, double scale, int lower_tail, int log_p);
        
  /** scale = 1/rate */
  double dexp(double x, double scale, int give_log);
  double pexp(double q, double scale, int lower_tail, int log_p);
  double qexp(double p, double scale, int lower_tail, int log_p);

  double dgeom(double x, double prob, int give_log);
  double pgeom(double q, double prob, int lower_tail, int log_p);
  double qgeom(double p, double prob, int lower_tail, int log_p);

  double dhyper(double x, double m, double n, double k, int give_log);
  double phyper(double q, double m, double n, double k, int lower_tail, int log_p);
  double qhyper(double p, double m, double n, double k, int lower_tail, int log_p);

  double dnbinom(double x, double size, double prob, int give_log);
  double pnbinom(double q, double size, double prob, int lower_tail, int log_p);
  double qnbinom(double p, double size, double prob, int lower_tail, int log_p);

  double dnbinom_mu(double x, double size, double mu, int give_log);
  double pnbinom_mu(double q, double size, double mu, int lower_tail, int log_p);

  double dpois(double x, double lambda, int give_log);
  double ppois(double x, double lambda, int lower_tail, int log_p);
  double qpois(double p, double lambda, int lower_tail, int log_p);

  double dweibull(double x, double shape, double scale, int give_log);
  double pweibull(double q, double shape, double scale, int lower_tail, int log_p);
  double qweibull(double p, double shape, double scale, int lower_tail, int log_p);

  double dlogis(double x, double location, double scale, int give_log);
  double plogis(double q, double location, double scale, int lower_tail, int log_p);
  double qlogis(double p, double location, double scale, int lower_tail, int log_p);

  double ptukey(double q, double nranges, double nmeans, double df, int lower_tail, int log_p);
  double qtukey(double p, double nranges, double nmeans, double df, int lower_tail, int log_p);
}
