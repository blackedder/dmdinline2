module rtod2.matrix2;

pragma(lib, "libgretl-1.0");
pragma(lib, "libR");

import rtod2.r2, rtod2.gretl2, rtod2.utils2;
import std.conv, std.stdio, std.string;

private struct matrix_info {
  int t1;
  int t2;
  char **colnames;
  char **rownames;
}

struct DoubleMatrix {
  int rows;
  int cols;
  double * val;
  matrix_info * info;

  DoubleMatrix * matptr() {
    return &this;
  }

  double opIndex(int r, int c) {
    return val[c*rows+r];
  }

  void opIndexAssign(double v, int r, int c) {
    val[c*rows+r] = v;
  }

  // This copies!
  void opAssign(DoubleMatrix m) {
    foreach(ii; 0..rows*cols) {
      val[ii] = m.val[ii];
    }
  }

  GretlMatrix opBinary(string op)(double a) {
    static if (op == "+") {
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] + a; }
      return result;
    }
    static if (op == "-") {
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] - a; }
      return result;
    }
    static if (op == "*") {
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] * a; }
      return result;
    }
    static if (op == "/") {
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] / a; }
      return result;
    }
  }

  GretlMatrix opBinaryRight(string op)(double a) {
    static if (op == "+") { 
      return this+a; 
    }
    static if (op == "-") {
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = a - this.val[ii]; }
      return result;
    }
    static if (op == "*") { 
      return this*a; 
    }
    static if (op == "/") {
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = a / this.val[ii]; }        
      return result;
    }
  }

  GretlMatrix opBinary(string op)(DoubleMatrix m) {
    static if (op == "+") {
      assertR(this.rows == m.rows, "Different number of rows in matrix addition"); 
      assertR(this.cols == m.cols, "Different number of columns in matrix addition");
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] + m.val[ii]; }
      return result;
    }
    static if (op == "-") {
      assertR(this.rows == m.rows, "Different number of rows in matrix subtraction"); 
      assertR(this.cols == m.cols, "Different number of columns in matrix subtraction");
      auto result = GretlMatrix(this.rows, this.cols);
      foreach(ii; 0..rows*cols) { result.val[ii] = this.val[ii] - m.val[ii]; }
      return result;
    } 
    static if (op == "*") {
      assertR(this.cols == m.rows, "Dimensions do not match for matrix multiplication");
      auto result = GretlMatrix(this.rows, m.cols);
      gretl_matrix_multiply(this.matptr, m.matptr, result.matptr);
      return result;
    }
  }
}

GretlMatrix dup(DoubleMatrix gm) {
  GretlMatrix result;
  result.matptr = gretl_matrix_copy(gm.matptr);
  result.mat.rows = gm.rows;
  result.mat.cols = gm.cols;
  result.mat.val = result.matptr.val;
  return result;
}

void print(DoubleMatrix m, string msg="") {
  gretl_matrix_print(&m, toStringz(msg));
}

GretlMatrix copyRows(DoubleMatrix m, int r0, int r1) {
  auto result = GretlMatrix(r1-r0, m.cols);
  foreach(int row; r0..r1) {
    Row(result, row-r0) = Row(m, row);
  }
  return result;
}

struct GretlMatrix {
  DoubleMatrix mat;
  DoubleMatrix * matptr;
  bool free = true;

  private this(DoubleMatrix * gm) {
    mat.rows = gm.rows;
    mat.cols = gm.cols;
    mat.val = gm.val;
    mat.info = gm.info;
    matptr = gm;
  }

  this(int r, int c=1) {
    matptr = gretl_matrix_alloc(r, c);
    mat.rows = r;
    mat.cols = c;
    mat.val = matptr.val;
    mat.info = matptr.info;
  }

  // Copies
  this(DoubleMatrix rm) {
    matptr = gretl_matrix_alloc(rm.rows, rm.cols);
    mat.rows = rm.rows;
    mat.cols = rm.cols;
    mat.val = matptr.val;
    foreach(ii; 0..rm.rows*rm.cols) {
      mat.val[ii] = rm.val[ii];
    }
    mat.info = matptr.info;
  }

  RMatrix opCast(T: RMatrix)() {
    auto result = RMatrix(mat.rows, mat.cols);
    result.mat = this.mat;
    return result;
  }

  double * ptr() {
    return mat.val;
  }

  this(this) {
    free = false;
  }
    
  ~this() {
    if (free) {
      gretl_matrix_free(matptr); 
    }
  }

  alias mat this;
}

GretlMatrix dup(GretlMatrix gm) {
  GretlMatrix result;
  result.matptr = gretl_matrix_copy(gm.matptr);
  result.mat.rows = gm.rows;
  result.mat.cols = gm.cols;
  result.mat.val = result.matptr.val;
  result.mat.info = result.matptr.info;
  return result;
}

struct RMatrix {
  DoubleMatrix mat;
  Robj robj;
  bool unprotect = true;
  
  alias mat this;

  this(int r, int c) {
    Rf_protect(robj = Rf_allocMatrix(14, r, c));
    mat.val = REAL(robj);
    mat.rows = r;
    mat.cols = c;
  }

  this(Robj rm) {
    assertR(isMatrix(rm), "Constructing RMatrix from something not a matrix"); 
    assertR(isNumeric(rm), "Constructing RMatrix from something that is not numeric");
    robj = rm;
    mat.val = REAL(rm);
    mat.rows = Rf_nrows(rm);
    mat.cols = Rf_ncols(rm);
    unprotect = false;
  }

  // There is something wrong. Possibly a compiler bug. This works.
  GretlMatrix opCast(T: GretlMatrix)() {
    auto result = GretlMatrix(mat.rows, mat.cols);
    foreach(ii; 0..mat.cols) {
      Column(result, ii) = Column(mat, ii);
    }
    return result;
  }

  this(this) {
    unprotect = false;
  }

  ~this() { 
    if (unprotect) { 
      Rf_unprotect_ptr(robj); 
    } 
  }

  double * ptr() {
    return mat.val;
  }

  DoubleMatrix * matptr() {
    return &mat;
  }
}

RMatrix dup(RMatrix rm) { 
  RMatrix result;
  result.robj = Rf_protect(Rf_duplicate(rm.robj));
  result.val = REAL(result.robj);
  result.rows = rm.rows;
  result.cols = rm.cols;
  return result;
}

struct Row {
  DoubleMatrix mat;
  int row;
  double * data;
  int length;

  this(DoubleMatrix m, int r) {
    mat = m;
    row = r;
    data = m.val;
    length = m.cols;
  }

  double opIndex(int c) {
    assertR(c < length, "Row index out of bounds");
    return data[c*mat.rows+row];
  }

  void opIndexAssign(double val, int ii) {
    mat[row, ii] = val;
  }

  void opAssign(T)(T v) {
    assertR(this.length == v.length, "Attempting to copy into Row an object with the wrong number of elements");
    foreach(ii; 0..to!int(this.length)) {
      mat[row, ii] = v[ii];
    }
  }

  void opAssign(double x) {
    foreach(ii; 0..this.length) { 
      mat[row, ii] = x;
    }
  }

  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[mat.rows];
    length -= 1;
  }
}

struct Col {
  DoubleMatrix mat;
  int col;
  double * data;
  int length;

  this(DoubleMatrix m, int c) {
    mat = m;
    col = c;
    data = m.val;
    length = m.rows;
  }

  double opIndex(int r) {
    assertR(r < length, "Column index out of bounds");
    return data[col*mat.rows+r];
  }

  void opIndexAssign(double val, int ii) {
    mat[ii, col] = val;
  }

  void opAssign(T)(T v) {
    assertR(this.length == v.length, "Attempting to copy into Column an object with the wrong number of elements");
    foreach(ii; 0..to!int(this.length)) {
      mat[ii, col] = v[ii];
    }
  }
 
  void opAssign(double x) {
    foreach(ii; 0..this.length) { 
      mat[ii, col] = x;
    }
  }

  void opAssign(DoubleMatrix m) {
    assertR(length == m.rows, "Wrong number of elements to copy into Column");
    assertR(m.cols == 1, "Cannot copy into a Column from a matrix with more than one column");
    foreach(ii; 0..this.length) {
      mat[ii, col] = m[ii, 0];
    }
  }

  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[1];
    length -= 1;
  }
}

void print(Row r) {
  foreach(val; r) {
    writeln(val);
  }
}

void print(Col c) {
  foreach(val; c) {
    writeln(val);
  }
}

void fill(DoubleMatrix m, double z) { 
  foreach(col; ByColumn(m)) { 
    col = z;
  } 
}

GretlMatrix chol(DoubleMatrix m) {
  assertR(m.rows == m.cols, "Taking a Cholesky decomposition of a non-square matrix");
  GretlMatrix result = dup(m);
  int err = gretl_matrix_cholesky_decomp(result.matptr);
  assertR(err == 0, "Cholesky decomposition failed");
  return result;
}

GretlMatrix inv(DoubleMatrix m) {
  assertR(m.rows == m.cols, "Taking the inverse of a non-square matrix");
  GretlMatrix result = dup(m);
  int err = gretl_invert_matrix(result.matptr);
  assertR(err == 0, "Taking the inverse of a matrix failed");
  return result;
}

DoubleMatrix transpose(DoubleMatrix m) {
  auto result = GretlMatrix(m.cols, m.rows);
  int err = gretl_matrix_transpose(result.matptr, m.matptr);
  assertR(err == 0, "Taking the transpose of a matrix failed");
  return result;
}

double det(DoubleMatrix m) {
  GretlMatrix temp = dup(m);
  int err;
  double result = gretl_matrix_determinant(temp.matptr, &err);
  assertR(err == 0, "Taking the determinant of a matrix failed");
  return result;
}

double logdet(DoubleMatrix m) {
  GretlMatrix temp = dup(m);
  int err;
  double result = gretl_matrix_log_determinant(temp.matptr, &err);
  assert(err == 0, "Taking the log determinant of a matrix failed");
  return result;
}

// This might change to return a DoubleVector.
GretlMatrix diag(DoubleMatrix m) {
  assertR(m.rows == m.cols, "diag is not intended to be used to take the diagonal of a non-square matrix");
  auto result = GretlMatrix(m.rows, 1);
  foreach(ii; 0..m.rows) { 
    result[ii,0] = m[ii,ii]; 
  }
  return result;
}

void setDiagonal(DoubleMatrix m, DoubleMatrix newdiag) {
  assertR(newdiag.cols == 1, "Cannot set a diagonal to a matrix with more than one column");
  assertR(m.rows == m.cols, "Cannot set the diagonal of a non-square matrix");
  assertR(m.rows == newdiag.rows, "Wrong number of elements in the new diagonal");
  foreach(ii; 0..newdiag.rows) { 
    m[ii,ii] = newdiag[ii,0]; 
  }
}

void setDiagonal(T)(DoubleMatrix m, T v) {
  assertR(m.rows == m.cols, "Cannot set the diagonal of a non-square matrix");
  assertR(m.rows == v.length, "Wrong number of elements in the new diagonal");
  foreach(ii; 0..to!int(v.length)) { 
    m[ii,ii] = v[ii]; 
  }
}

void setDiagonal(DoubleMatrix m, double v) {
  assertR(m.rows == m.cols, "Attempting to set the diagonal of a non-square matrix");
  foreach(ii; 0..m.rows) { 
    m[ii,ii] = v; 
  }
}

double trace(DoubleMatrix m) { 
  assertR(m.rows == m.cols, "Cannot take the trace of a non-square matrix");
  return gretl_matrix_trace(m.matptr); 
}

// Raise each element to a power.
GretlMatrix raise(DoubleMatrix m, double k) {
  GretlMatrix result = dup(m);
  gretl_matrix_raise(result.matptr, k);
  return result;
}

// Power of a matrix.
GretlMatrix pow(DoubleMatrix m, int s) {
  assertR(s >= 0, "Exponent on matrix has to be a positive integer. Consider using raise instead of pow.");
  assertR(m.rows == m.cols, "Attempting to exponentiate a non-square matrix. Consider using raise instead of pow.");
  int err;
  auto temp = gretl_matrix_pow(m.matptr, s, &err);
  assertR(err == 0, "Exponentiating a matrix failed");
  return GretlMatrix(temp);
}

GretlMatrix solve(DoubleMatrix x, DoubleMatrix y) {
  auto temp = dup(x);
  auto result = dup(y);
  int err = gretl_LU_solve(temp.matptr, result.matptr);
  assertR(err == 0, "Call to solve failed");
  return result;
}

GretlMatrix eye(int k) {
  auto result = GretlMatrix(k, k);
  result.fill(0.0);
  foreach(ii; 0..k) { 
    result[ii,ii] = 1.0;
  }
  return result;
}

DoubleMatrix kron(DoubleMatrix x, DoubleMatrix y) {
  auto result = GretlMatrix(x.rows*y.rows, x.cols*y.cols);
  int err = gretl_matrix_kronecker_product(x.matptr, y.matptr, result.matptr);
  assertR(err == 0, "Kronecker product failed");
  return result;
}

private struct Observation {
  double value;
  int obs;
}

GretlMatrix sort(bool inc=true)(DoubleMatrix m, int col) {
  // Put all elements and row numbers in an array
  Observation[] temp;
  foreach(int ii, val; Col(m,col)) { 
    temp[ii] ~= Observation(val, ii); 
  }

  // Sort the array according to data
  static if (inc) {
    bool com(Observation x, Observation y) { return x.value < y.value; }
  } else {
    bool com(Observation x, Observation y) { return x.value > y.value; }
  }
  auto output = sort!(com)(temp);

  // Now put the results into a new matrix
  auto result = GretlMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows) { Row(result, ii) = Row(m, output[ii].value); }
  return result;
}

int[] order(bool inc=true)(DoubleMatrix m, int col) {
  Observation[] temp;
  foreach(int ii, val; Col(m,col)) { 
    temp[ii] ~= Observation(val, ii); 
  }

  // Sort the array according to data
  static if (inc) {
    bool com(Observation x, Observation y) { return x.value < y.value; }
  } else {
    bool com(Observation x, Observation y) { return x.value > y.value; }
  }
  auto output = sort!(com)(temp);
  return output.array;
}

struct ByRow {
  DoubleMatrix mat;
  int rowno;
  int length;
  
  this(DoubleMatrix m) {
    mat = m;
    rowno = 0;
    length = m.rows;
  }

  bool empty() { 
    return length == 0; 
  }
  
  Row front() { 
    return Row(mat, rowno); 
  }

  void popFront() {
    rowno += 1;
    length -= 1;
  }
}

struct ByColumn {
  DoubleMatrix mat;
  int colno;
  int length;

  this(DoubleMatrix m) {
    mat = m;
    colno = 0;
    length = m.cols;
  }

  bool empty() { 
    return length == 0; 
  }
  
  Col front() { 
    return Col(mat, colno); 
  }
  
  void popFront() {
    colno += 1;
    length -= 1;
  }
}
