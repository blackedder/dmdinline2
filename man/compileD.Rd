\name{compileD}
\alias{compileD}
\title{Compile and load a D shared library}
\description{Compiles D code, creates a shared library, and loads it from within an R program}
\usage{compileD(libname, code, config="~/.rtod2/config.R", pre="")}
\arguments{
	\item{libname}{Name of the library (without the "lib" prefix, i.e., "foo" to create libfoo.so)}
	\item{code}{String holding the D functions that will be called from R}
	\item{config}{Location of the configuration file}
	\item{pre}{String holding any D code that will not be called from R. This will normally consist of import statements and helper functions.}
	\item{extra.flags}{String holding any extra compile flags that will be passed to dmd. This can consist of extra import paths for different libraries.}
}
\value{Prints compilation output, along with any error messages from the DMD compiler.}
\author{Lance Bachmeier}
\examples{
txt <- '
Robj testfun(Robj rx, Robj ry) {
  double x = rx.scalar;
  double y = ry.scalar;
  double z = x+y;
  return z.robj;
}'
compileD("foo", txt)
.Call("testfun", 3.5, -2.6)
}
