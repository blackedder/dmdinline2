# Note on dmdinline

Please use this new package rather than dmdinline. The original dmdinline is around for legacy purposes.

# Installation on Linux

You need to install the `devtools` package and the [DMD compiler](http://dlang.org/download.html). Then you can install the library:

```
sudo R
library(devtools)
install_bitbucket("bachmeil/dmdinline2")
library(dmdinline2)
install_librtod2()
```

# Installation on OSes Other than Linux

The library will currenly only run on Linux. If you use another OS, I recommend using Docker, as that works for me. In particular, I recommend using the [rocker/rstudio image](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image), with folder sharing as explained on that page. 

Once you've got the RStudio Server running, follow the instructions under "Dependencies external to the R system" to install DMD, the Gretl libraries, and dmdinline2:

```
docker ps
docker exec -it <container-id> bash # Use the id from the previous step
apt-get update
apt-get install libgretl1-dev libxml2-dev
cd /home/rstudio
mkdir Downloads
cd Downloads
wget http://downloads.dlang.org/releases/2.x/2.067.1/dmd_2.067.1-0_amd64.deb
dpkg --install dmd_2.067.1-0_amd64.deb
R # Will enter R as root user
install.packages("devtools")
library(devtools)
install_bitbucket("bachmeil/dmdinline2")
library(dmdinline2)
install_librtod2()
q() # Answer to exit R
exit
```

Then do a commit of the Docker image so that it's permanent. Instructions are at the bottom of [this page](http://docs.docker.com/articles/basics/), but basically you enter a single line of this form:

```
docker commit <container_id> <some_name>
```

Doesn't this approach suck? Of course it does. It would be better to create a proper Docker image and push it to the Docker registry. Thank you for volunteering to do it. 

Even better would be getting dmdinline2 to work on all OSes. Unfortunately I know nothing about Windows development, and D doesn't yet have shared library support on Mac OS X, so that's not likely to happen. I'll be more than happy to merge your work if you want to do it. (It actually shouldn't take much for someone that understands D development on Windows.)

# Hello World

Here's the Hello World example for dmdinline:

```
library(dmdinline2)
code <- '
  import std.stdio;

  Robj hello() {
    writeln("Hello, World!");
    return RNil;
  }
'
compileD("foo", code)
.Call("hello")
```

# Examples

Some additional examples can be found [here](http://lancebachmeier.com/dmdinline-examples/).

# Bugs/Issues/Comments

Please open an issue for anything that doesn't work or for any other communications.